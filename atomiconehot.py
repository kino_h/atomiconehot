from pymatgen.core.composition import Composition
from pymatgen.core.periodic_table import Element


class AtomicOneHot:
    def __init__(self,z=None):
        self.set_features() # to make self.features
        if z is None:
            self.set_zero()
        else:
            self.set_z(z)
    
    def find_valence(self,es):
        out_most_ne= {"s":0, "p":0, "d":0, "f":0 }
        #out_most_n= {"s":0, "p":0, "d":0, "f":0 }
        for n,l,ne in es:
            out_most_ne[l] = ne
            #out_most_n[l] = n

        return out_most_ne

    def set_features(self):
        features = []
        for i in range(2):
            features.append("s{}".format(i+1))
        for i in range(6):
            features.append("p{}".format(i+1))
        for i in range(10):
            features.append("d{}".format(i+1))
        for i in range(14):
            features.append("f{}".format(i+1))
        for i in range(6):
            features.append("row{}".format(i+1))
        self.features = features
        self.columns = self.features
        
    def dic2vec(self,v):
        xlist = []
        for f in self.features:
            xlist.append(v[f])
        return xlist
    
    def set_dic_zero(self):
        onehotdic = {}
        for f in self.features:
            onehotdic[f] = 0
        return onehotdic
    def set_zero(self):
        onehotdic = self.set_dic_zero()
        return self.dic2vec(onehotdic)
        
    def set_z(self,z):
        z = int(z)
        elm = Element("H")
        elm = elm.from_Z(z)
        #print(elm.full_electronic_structure)

        """
        2 He 18
        10 Ne 18
        18 Ar 18
        36 Kr 18
        54 Xe 18
        86 Rn 18
        """
        elemcore = []
        if z >2 and z<=10:
            elemcore = Element("He").full_electronic_structure
        elif z>10 and z<=18:
            elemcore = Element("Ne").full_electronic_structure
        elif z>18 and z<=38:
            elemcore = Element("Ar").full_electronic_structure
        elif z>38 and z<=54:
            elemcore = Element("Kr").full_electronic_structure
        elif z>54 and z<=86:
            elemcore = Element("Xe").full_electronic_structure
        elif z>86:
            elemcore = Element("Rn").full_electronic_structure

        elemvalence = set(elm.full_electronic_structure) - set(elemcore)
        out_most_ne = self.find_valence(elemvalence)
        #print(out_most_ne)
        
        atomichotv = self.set_dic_zero()
        
        if elm.is_lanthanoid or elm.is_actinoid:
            use_f = True
        else:
            use_f = False
            
        print(out_most_ne)
        for k in out_most_ne.keys():

            # This version doesn't use f if it is not lanthanoid or actinoid
            if k=="f" and not use_f:
                continue
                
            x = out_most_ne[k]
            s = "{}{}".format(k,x)
            atomichotv[s] = 1

        atomichotv["row{}".format(elm.row)] = 1
        #print(z,atomichotv)
        self.onehot = self.dic2vec(atomichotv)
    
atomichotvlist = []

for z in zlist:
    #print("z",z)
    atomichotv = AtomicOneHot(z).onehot
    #print(atomichotv)
    #print("=--------------")
    #print(len(atomichotv))
    atomichotvlist.append(atomichotv)

